package godau.fynn.horndirect;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import androidx.annotation.Nullable;
import com.ortiz.touchview.TouchImageView;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import godau.fynn.librariesdirect.AboutLibrariesActivity;
import godau.fynn.librariesdirect.AboutLibrariesConfig;
import godau.fynn.librariesdirect.Library;
import godau.fynn.librariesdirect.License;

public class MainActivity extends Activity {

    private TouchImageView imageView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        imageView = findViewById(R.id.image);

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageView.setImageDrawable(null);
                imageView.setZoom(1f);
                loadImage();
            }
        });

        imageView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {

                AboutLibrariesConfig.setLibraries(new Library[]{
                        new Library("TouchImageView", License.MIT_LICENSE, null, "Michael Ortiz", "https://github.com/MikeOrtiz/TouchImageView"),
                        new Library("Picasso", License.APACHE_20_LICENSE, null, "Square, Inc.", "https://square.github.io/picasso/"),
                        new Library("librariesDirect", License.CC0_LICENSE, null, "Fynn Godau", "https://codeberg.org/fynngodau/librariesDirect"),
                });
                startActivity(new Intent(MainActivity.this, AboutLibrariesActivity.class));

                return true;
            }
        });

        loadImage();
    }

    private void loadImage() {
        Picasso.get().load("https://flauschehorn.sexy/api.php").memoryPolicy(MemoryPolicy.NO_CACHE).into(imageView);
    }
}
